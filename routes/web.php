<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('404', function () {
    return view('404');
});

Route::get('about', function () {
    return view('about');
});

Route::get('add-property', function () {
    return view('add-property');
});

Route::get('agencies-details', function () {
    return view('agencies-details');
});

Route::get('agencies-listing-1', function () {
    return view('agencies-listing-1');
});

Route::get('agencies-listing-1', function () {
    return view('agencies-listing-1');
});

Route::get('agencies-listing-2', function () {
    return view('agencies-listing-2');
});

Route::get('agent-details', function () {
    return view('agent-details');
});

Route::get('agents-listing-grid', function () {
    return view('agents-listing-grid');
});

Route::get('agents-listing-row-2', function () {
    return view('agents-listing-row-2');
});

Route::get('agents-listing-row', function () {
    return view('agents-listing-row');
});

Route::get('blog-details', function () {
    return view('blog-details');
});

Route::get('blog-full-grid', function () {
    return view('blog-full-grid');
});

Route::get('blog-full-list', function () {
    return view('blog-full-list');
});

Route::get('blog-grid-sidebar', function () {
    return view('blog-grid-sidebar');
});

Route::get('blog-list-sidebar', function () {
    return view('blog-list-sidebar');
});

Route::get('change-password', function () {
    return view('change-password');
});

Route::get('coming-soon', function () {
    return view('coming-soon');
});

Route::get('contact-us', function () {
    return view('contact-us');
});

Route::get('dashboard', function () {
    return view('dashboard');
});

Route::get('faq', function () {
    return view('faq');
});

Route::get('favorited-listings', function () {
    return view('favorited-listings');
});

Route::get('index-2', function () {
    return view('index-2');
});

Route::get('index-3', function () {
    return view('index-3');
});

Route::get('index-4', function () {
    return view('index-4');
});

Route::get('index-5', function () {
    return view('index-5');
});

Route::get('index-6', function () {
    return view('index-6');
});

Route::get('index-7', function () {
    return view('index-7');
});

Route::get('index-8', function () {
    return view('index-8');
});

Route::get('index-9', function () {
    return view('index-9');
});

Route::get('index-10', function () {
    return view('index-10');
});

Route::get('index-11', function () {
    return view('index-11');
});

Route::get('index-12', function () {
    return view('index-12');
});

Route::get('index-13', function () {
    return view('index-13');
});

Route::get('index-14', function () {
    return view('index-14');
});

Route::get('index-15', function () {
    return view('index-15');
});

Route::get('index-16', function () {
    return view('index-16');
});

Route::get('index-17', function () {
    return view('index-17');
});

Route::get('index-18', function () {
    return view('index-18');
});

Route::get('index-19', function () {
    return view('index-19');
});

Route::get('index-20', function () {
    return view('index-20');
});

Route::get('index-21', function () {
    return view('index-21');
});

Route::get('index-22', function () {
    return view('index-22');
});

Route::get('index-23', function () {
    return view('index-23');
});

Route::get('index-24', function () {
    return view('index-24');
});

Route::get('invoice', function () {
    return view('invoice');
});

Route::get('invoice', function () {
    return view('invoice');
});

Route::get('my-listings', function () {
    return view('my-listings');
});

Route::get('payment-method', function () {
    return view('payment-method');
});

Route::get('pricing-table', function () {
    return view('pricing-table');
});

Route::get('properties-full-grid-1', function () {
    return view('properties-full-grid-1');
});

Route::get('properties-full-grid-2', function () {
    return view('properties-full-grid-2');
});

Route::get('properties-full-grid-3', function () {
    return view('properties-full-grid-3');
});

Route::get('properties-full-list-1', function () {
    return view('properties-full-list-1');
});

Route::get('properties-full-list-2', function () {
    return view('properties-full-list-2');
});

Route::get('properties-grid-1', function () {
    return view('properties-grid-1');
});

Route::get('properties-grid-2', function () {
    return view('properties-grid-2');
});

Route::get('properties-grid-3', function () {
    return view('properties-grid-3');
});

Route::get('properties-grid-4', function () {
    return view('properties-grid-4');
});

Route::get('properties-half-map-1', function () {
    return view('properties-half-map-1');
});

Route::get('properties-half-map-2', function () {
    return view('properties-half-map-2');
});

Route::get('properties-half-map-3', function () {
    return view('properties-half-map-3');
});

Route::get('properties-list-1', function () {
    return view('properties-list-1');
});

Route::get('properties-list-2', function () {
    return view('properties-list-2');
});

Route::get('properties-top-map-1', function () {
    return view('properties-top-map-1');
});

Route::get('properties-top-map-2', function () {
    return view('properties-top-map-2');
});

Route::get('properties-top-map-3', function () {
    return view('properties-top-map-3');
});

Route::get('register', function () {
    return view('register');
});

Route::get('shop-checkout', function () {
    return view('shop-checkout');
});

Route::get('shop-full-page', function () {
    return view('shop-full-page');
});

Route::get('shop-order', function () {
    return view('shop-order');
});

Route::get('shop-single', function () {
    return view('shop-single');
});

Route::get('shop-with-sidebar', function () {
    return view('shop-with-sidebar');
});

Route::get('single-property-1', function () {
    return view('single-property-1');
});

Route::get('single-property-2', function () {
    return view('single-property-2');
});

Route::get('single-property-3', function () {
    return view('single-property-3');
});

Route::get('single-property-4', function () {
    return view('single-property-4');
});

Route::get('single-property-5', function () {
    return view('single-property-5');
});

Route::get('single-property-6', function () {
    return view('single-property-6');
});

Route::get('submit-property', function () {
    return view('submit-property');
});

Route::get('ui-element', function () {
    return view('ui-element');
});

Route::get('under-construction', function () {
    return view('under-construction');
});

Route::get('user-profile', function () {
    return view('user-profile');
});

